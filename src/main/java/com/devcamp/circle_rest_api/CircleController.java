package com.devcamp.circle_rest_api;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class CircleController {
  @GetMapping("/circle-area")
  public ArrayList<Circle> getCircleArea(){
    //thực hiện khởi tạo đối tượng hình tròn (có tham số) truyền vào các tham số radius
    Circle circle1 = new Circle(10);
    Circle circle2 = new Circle(20);
    //Sử dụng phương thức toString để in 2 đối tượng trên ra console
    System.out.println("Circle 1: " + circle1.toString());
    System.out.println("Circle 2: " + circle2.toString());
    //tạo danh sách cho circle
    ArrayList<Circle> listcircle = new ArrayList();
    listcircle.add(circle1);
    listcircle.add(circle2);

    return listcircle;
  }
}
