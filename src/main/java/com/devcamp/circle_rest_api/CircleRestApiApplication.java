package com.devcamp.circle_rest_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CircleRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CircleRestApiApplication.class, args);
	}

}
